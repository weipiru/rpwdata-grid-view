package com.rpw.utils

import android.content.Context

internal class Utils {
    companion object{
        /**获取屏幕的宽度（像素） */
        fun getScreenWidth(context: Context): Int {
            return context.resources.displayMetrics.widthPixels //1080
        }
    }
}