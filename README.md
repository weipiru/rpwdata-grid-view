# RPWDataGridView

#### 介绍
一个超级简洁的表格控件，支持固定多列，目前支持基本数据加载，点击事件

![Preview](./preview.gif)

#### 使用说明

自行打包依赖

1. `RPWDataGridView<T>` 表格控件 T为数据类型
1. 主要接口
 + `build(...)` 添加列
 + `setRowBuildListener`  行数据构建接口，在这设置每列显示的数据
 + `setDataSource` 设置数据源

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
